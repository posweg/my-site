<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Pòsweg</title>

	<link rel="shortcut icon" href="favicon.ico" />

    <!-- JavaScript -->
        <!-- jQuery -->
        <script src="js/jquery-3.1.1.js"></script>
        <!-- Smooth Scroll -->
        <script src="js/smoothscroll.js"></script>
        <!-- Scroll Magic -->
        <script src="js/scrollmagic/plugins/jquery.ScrollMagic.js"></script>
        <script src="js/scrollmagic/plugins/debug.addIndicators.js"></script>
        <!-- GSAP -->
        <script src="js/gsap/TweenMax.js"></script>
        <!-- Magnific-Popup -->
        <link rel="stylesheet" href="js/magnific-popup.css">
        <script src="js/jquery.magnific-popup.js"></script>

    <link rel="stylesheet" href="style/main.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>

    <!-- top bar -->
    <div id="banner">
        <div id="menu-button"><img src="images/menu.png"></div>
        <!--<img src="images/avi.png" id="avi">-->
        <div id="spacer"></div>
        <h1>Pòsweg</h1>
        <a href="login"><div id="login">Sign in</div></a>
    </div>

    <!-- sub-bar -->
    <div id="frame">
        <table id="menu">
            <tr><td>
                <a href="#" id="bHome"><div class="buttons-menu">Home</div></a>
            </td></tr>

            <tr><td>
                <a href="#" id="bGallery"><div class="buttons-menu">Gallery</div></a>
            </td></tr>

            <tr><td>
                <a href="#" id="bProductivity"><div class="buttons-menu">Productivity</div></a>
            </td></tr>

            <tr><td>
                <a ><div class="buttons-menu">Music</div></a>
            </td></tr>

            <tr><td>
                <a ><div class="buttons-menu">Links</div></a>
            </td></tr>

            <tr><td>
                <a href="php-test/index.php"><div class="buttons-menu">PHP</div></a>
            </td></tr>

        </table>
        <!--<img src="images/pikachu-chari.png">-->
    </div>

    <!-- It me -->
    <!--<img src="images/hoo.png" id="itMe">-->

    <!-- Main body -->
    <div id="home"></div>
    <div id="body">
        <div>
            <h1>Welcome to my website!</h1>
            <br>
            <p>Hi, I'm Joshua AKA Pòsweg, a highschool student that likes to teach
                himself a lot of different things in his spare time.</p>
            <br>
            <p>I'm a technology enthusiast, I love drawing, programming and making music although
                I'm just starting. I also like doing things with other people, so don't doubt
                to contact me on Twitter, where I am the most of the time.</p>
            <br>
            <p>In this website you will find all the stuff I made, things that I'm currently
                working on and some of the techniques that I use to be productive.</p>
        </div>
        <img id="img-1" src="images/avi-2.png">
    </div>

    <div id="gallery"></div>
    <div id="gallery-section">
        <div>
			<h1>Gallery</h1>
            <br>
            <p>	Work in progress. </p>
              </br>

              <!--
              <form action="php-test/upload.php" method="POST" enctype="multipart/form-data">
              	<input type="file" name="file">
              	<button type="submit" name="submit">Upload image</button>
              </form>-->

              <div class="container">
                  <div class="gallery">
                      <?php
                      //Include database configuration file
              				//DB details
              				$dbHost = 'localhost';
              				$dbUsername = 'root';
              				$dbPassword = '';
              				$dbName = 'mainDB';

              				$db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

              				if ($db->connect_error) {
              				    die("Unable to connect database: " . $db->connect_error);
              				}

              	      $query = $db->query("SELECT * FROM images ORDER BY date DESC");

                      if($query->num_rows > 0){
                          while($row = $query->fetch_assoc()){
                              $imageURL = 'php-test/uploads/'.$row["name"];
                      ?>
                          <img src="<?php echo $imageURL; ?>" alt="<?php $imageURL ?>" style="width: 100px;" />
                      <?php  }
              			} ?>
                  </div>
              </div>
        </div>
    </div>

    <!--<div id="productivity"></div>
    <div id="productivity-section">
        <div>
        <h1>Productivity</h1>
            <br>
            <table id="productivity-table"></table>
        </div>
    </div>-->

    <script src="js/main.js"></script>
</body>
</html>
