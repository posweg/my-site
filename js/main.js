var ch = $('#menu-button').height();
$('#menu-button').css({
    'width': ch + 'px'
});
$('#menu-button').css({
    'margin': ch/3 + 'px'
});

$(document).ready(function(){

    $("#menu-button").click(function(){
        $("#frame").toggleClass('animate');
        $("#menu-button img").toggleClass('mantenuto');
        $(this).addClass('clicked');
        setTimeout(function(){
            $("#menu-button").removeClass('clicked');
        },200);
    });

    $("#bHome").click(function(){
        document.querySelector('#home').scrollIntoView({ behavior: 'smooth' });
        setTimeout(function(){
            window.location.href = "#home";
        },500);
    });
    $("#bGallery").click(function(){
        document.querySelector('#gallery').scrollIntoView({ behavior: 'smooth' });
        setTimeout(function(){
            window.location.href = "#gallery";
        },500);
    });
    /* Productivity section scroll */
    /*$("#bProductivity").click(function(){
        document.querySelector('#productivity').scrollIntoView({ behavior: 'smooth' });
        setTimeout(function(){
            window.location.href = "#productivity";
        },500);
    });*/

    var mediaquery = window.matchMedia("(max-width: 768px)");
    if (mediaquery.matches) {
            $(".buttons-menu").click(function(){
            $("#frame").toggleClass('animate');
            $("#menu-button img").toggleClass('mantenuto');
            $("#menu-button").removeClass('clicked');
            /* Acuérdate de que tienes que hacer la animación del click, eh. */
        });
    }
    
    $('.gallery-popup').magnificPopup({
        type: 'image',
        closeBtnInside: false,
        closeOnContentClick: true,
        mainClass: 'mfp-no-margins mfp-with-zoom',
        image: {
          verticalFit: true
        },
        image: {
          verticalFit: true
        },
        /*zoom: {
          enabled: true,
          duration: 300 // don't foget to change the duration also in CSS
        }*/
    });
});
